cookiecutter-flask
==================

A Flask template for cookiecutter_.

.. _cookiecutter: https://github.com/audreyr/cookiecutter

Features
--------

- Twitter Bootstrap 3 and starter templates
- flask-mongoengine - Object-Document-Mapper for working with MongoDB
- Procfile for deploying to a PaaS (e.g. Heroku)
- nose for testing

Using this template
-------------------
::

    $ pip install cookiecutter
    $ cookiecutter https://bitbucket.org/jsmitka/cookiecutter-flask-mongodb.git

You will be asked about your basic info (name, project name, etc.). This info will be used in your new project.


License
-------
BSD licensed.

